resource "openstack_compute_floatingip_v2" "my_floatingip" {
  pool = "name_of_the_pool"
}

resource "openstack_compute_secgroup_v2" "my_security_group" {
  name        = "My Security Group"
  description = "This is a security group I have made"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 8080
    to_port     = 8080
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}

resource "openstack_compute_keypair_v2" "my_key_pair" {
  name       = "My Access Key"
  public_key = "${file("./ed25519_example_key.pub")}"
}

resource "openstack_compute_instance_v2" "my_instance" {
  name            = "My Instance"
  image_name      = "CentOS 7"
  flavor_name     = "m1.small"
  security_groups = ["${openstack_compute_secgroup_v2.my_security_group.name}"]
  key_pair        = "${openstack_compute_keypair_v2.my_key_pair.name}"
}

resource "openstack_compute_floatingip_associate_v2" "my_floatingip_associate" {
  floating_ip = "${openstack_compute_floatingip_v2.my_floatingip.address}"
  instance_id = "${openstack_compute_instance_v2.my_instance.id}"
}
