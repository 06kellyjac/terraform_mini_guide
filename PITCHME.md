# Mini Terraform Guide

![Terraform Logo](images/logo_terraform_full_white.png)

---

With a small focus on the provider

![OpenStack Logo](images/logo_openstack.png)

---

## Installation

* Download Terraform from the Terraform website:
  * <https://www.terraform.io/downloads.html>
* Add a new directory to your `$PATH` variable and put Terraform within it. Or use a directory already set up.
* Done!

---

## Best Practice

Terraform uses Hashicorp's HCL (Hashicorp Configuration Language) and the best source for best-practice with styling is:
<https://github.com/hashicorp/hcl/blob/master/README.md#syntax>

+++

### Indentation

* Use 2 Spaces rather than Tabs.
* Indent `=` symbols to be in line for clarity.
* `terraform fmt` can format your terraform configuration correctly for you.

```nginx
resource "openstack_compute_instance_v2" "my_instance" {
  name        = "My Instance"
  image_name  = "CentOS 7"
  flavor_name = "m1.small"
  key_pair    = "my_access_key"
}
```

+++

### Files

They are not _required_ to be called the following but generally you should have these 3 files:

* `main.tf` for your general terraform configuration.
* `variables.tf` for variable configuration.
* `outputs.tf` for output configuration.

---

## Configuration

There are over 50 Providers with different Resources to configure; they generally share similarities.
The Configuration options are similar to those you enter manually.
For details on your required Provider:
<https://www.terraform.io/docs/providers/index.html>

---

## Important Resources

* `openstack_compute_instance_v2`
* `openstack_compute_secgroup_v2`
* `openstack_compute_keypair_v2`
* `openstack_networking_floatingip_v2`

+++

### Main format of a Resource

```nginx
resource "resource_name" "given_name" {
  variable     = "value"
  array        = ["value0", "value1", "value2"]
  ref_resource = "${other_resource_name.other_given_name.something}"
  an_address   = "${other_resource_name.other_given_name.address}"
}

resource "other_resource_name" "other_given_name" {
  something = "Lorem ipsum dolor sit amet."

  # There can be computed variables that you can reference e.g
  # Lets say `other_resource_name` will compute a variable
  # named `address`. Even though it's not set.
  # It will be computed and I can refer to it.
}

```

+++

### @size[.99em](`openstack_compute_instance_v2`)

```nginx
resource "openstack_compute_instance_v2" "my_instance" {
  name            = "My Instance"
  image_name      = "CentOS 7"
  flavor_name     = "m1.small"
  security_groups = ["${openstack_compute_secgroup_v2.my_security_group.name}"]
  floating_ip     = "${openstack_compute_floatingip_v2.my_floating_ip.address}"
  key_pair        = "${openstack_compute_keypair_v2.my_key_pair.name}"
}
```

+++

### @size[.99em](`openstack_compute_secgroup_v2`)

```nginx
resource "openstack_compute_secgroup_v2" "my_security_group" {
  name        = "My Security Group"
  description = "This is a security group I have made"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 8080
    to_port     = 8080
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}
```

+++

### `openstack_compute_keypair_v2`

```nginx
resource "openstack_compute_keypair_v2" "my_key_pair" {
  name       = "My Access Key"
  public_key = "${file("./your_key.pub")}"
}
```

+++

### @size[.84em](`openstack_networking_floatingip_v2`)

```nginx
resource "openstack_compute_floatingip_v2" "my_floating_ip" {
  pool = "the_name_of_the_pool"
}
```

---

## Key Terraform Commands

In the directory where your `.tf` files are.
There are some key terraform commands:

* `terraform plan`
* `terraform show`
* `terraform apply`
* `terraform destroy`

+++

### Plan & Show

`terraform plan` is good for debugging.
It will plan out what resources it will generate without making them.
This is also important so that you don't waste resources.
<!-- markdownlint-disable MD035 -->
___
<!-- markdownlint-enable MD035 -->
`terraform show` will show what terraform is currently managing.

+++

### Apply & Destroy

`terraform apply` will have terraform generate all your resources.
At this stage it can also ask for variables you want manual input for.
<!-- markdownlint-disable MD035 -->
___
<!-- markdownlint-enable MD035 -->
`terraform destroy` will remove all terraform managed resources.
Use the `-force` to bypass the confirmation.

---

## Provider Authentication

Terraform requires authentication with your provider.
To do this you can **either** set up a provider resource **or** download your `openrc.sh` file.
On OpenStack: Make sure you are on the right project
@size[.8em](`Compute > Access & Security > API Access`) and then click `Download OpenStack RC File v3`

---

## OpenStack demo

* Open a terminal that supports `Bash`.
* git clone the terraform-guide project **or** download as a `.zip`
  * @size[.6em](`git@gitlab.com:06kellyjac/terraform_mini_guide.git`)
  * <https://gitlab.com/06kellyjac/terraform_mini_guide>
* cd into the terraform-guide folder.

+++

### Preparation

* Open `main.tf`
* Replace the `pool` variable `name_of_the_pool` with your pool name.
  * To find your pool name go to OpenStack `Network > Networks` and choose a pool.
* Add a public SSH key called `my_key.pub` to the the terraform-guide folder.
  * **or**
* edit `my_key.pub` in `main.tf` to a public key you already have in the directory.

+++

### Using terraform

1. Authenticate with:
  @size[.6em](`source your/openrc/location/my_openrc.sh`)
2. `terraform plan` to see what the script will make.
3. `terraform apply` to run terraform and wait for it to create the resources.
4. `terraform show` to see what terraform is managing.
5. `terraform destroy` when you're ready to kill those resources.
