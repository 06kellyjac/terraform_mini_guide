# Hashicorp Terraform Guide

[gitpitch_shield]: https://img.shields.io/badge/gitpitch-slideshow-brightgreen.svg?longCache=true&style=flat-square
[gitpitch_link]: https://gitpitch.com/06kellyjac/terraform_mini_guide/master?grs=gitlab

[pipeline_badge]: https://gitlab.com/06kellyjac/terraform_mini_guide/badges/master/pipeline.svg
[pipeline_link]: https://gitlab.com/06kellyjac/terraform_mini_guide/pipelines

[![GitPitch][gitpitch_shield]][gitpitch_link] [![Pipeline][pipeline_badge]][pipeline_link]

This is a short guide for teaching the basics of Terraform.

Click the Git Pitch badge above or [click here][gitpitch_link].

---
![Hashicorp](images/logo_hashicorp_full_black.png "Hashicorp Logo")
![Terraform](images/logo_terraform_full.png "Terraform Logo")
